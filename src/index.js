import React from 'react'
import Moment from 'react-moment'
import moment from 'moment'
import lodash from 'lodash'
import 'moment/locale/ru'
import './index.sass'

class ReactRangeDatepicker extends React.Component {
  state = {
    focusedInput: null,
    monthTable: [[], [], [], [], [], []],
    selectedDate: new Date(),
    selectedMonth: new Date(new Date().setDate(1)),
    dateTimeStart: new Date(),
    dateTimeEnd: new Date(),
    timeStringStart: '',
    timeStringEnd: '',
    calendarShow: false
  }

  constructor(props) {
    super(props)
    this.setWrapperRef = this.setWrapperRef.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
    if (this.props.dateTimeStart) {
      this.setState({
        dateTimeStart: new Date(this.props.dateTimeStart),
        timeStringStart: this.props.onlyDate ?
          this.getDateInString(new Date(this.props.dateTimeStart)) :
          this.getTimeInString(new Date(this.props.dateTimeStart))
      })
    }

    if (this.props.dateTimeEnd) {
      this.setState({
        dateTimeEnd: new Date(this.props.dateTimeEnd),
        timeStringEnd: this.props.onlyDate ?
          this.getDateInString(new Date(this.props.dateTimeEnd)) :
          this.getTimeInString(new Date(this.props.dateTimeEnd))
      })
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  setWrapperRef(node) {
    this.wrapperRef = node
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ calendarShow: false })
    }
  }

  buildMonthTable() {
    const d = new Date(this.state.selectedMonth)
    const daysOfSelectedMonth =
      new Date(d.getFullYear(), d.getMonth() + 1, 0).getDate()
    const daysOfPastMonth =
      new Date(d.getFullYear(), d.getMonth(), 0).getDate()
    let firstDayOfMonth = d.getDay() - 1 === -1 ? 6 : d.getDay() - 1 || 7

    let count = 1
    let newMonthCount = 1
    let tempArray = []

    this.setState(
      { monthTable: [[], [], [], [], [], []] },
      () => {
        this.state.monthTable.forEach((row, i) => {
          if (i === 0) {
            for (let j = firstDayOfMonth; j > 0; j -= 1) {
              row.push(
                new Date(d).setMonth(d.getMonth() - 1, daysOfPastMonth - j + 1)
              )
              tempArray = this.state.monthTable
              tempArray[i] = row
              this.setState({ monthTable: tempArray })
            }

            while (row.length < 7) {
              row.push(new Date(d).setDate(count))
              tempArray = this.state.monthTable
              tempArray[i] = row
              this.setState({ monthTable: tempArray })
              count += 1
            }
          } else {
            for (let j = 0; j < 7; j += 1) {
              if (count <= daysOfSelectedMonth) {
                row.push(new Date(d).setDate(count))
                tempArray = this.state.monthTable
                tempArray[i] = row
                this.setState({ monthTable: tempArray })
                count += 1
              } else {
                row.push(new Date(d).setMonth(d.getMonth() + 1, newMonthCount))
                tempArray = this.state.monthTable
                tempArray[i] = row
                this.setState({ monthTable: tempArray })
                newMonthCount += 1
              }
            }
          }
        })
      }
    )
  }

  setTime(time) {
    const h = time.split(':')[0]
    const m = time.split(':')[1]

    this.setSelectedDate(new Date(this.state.selectedDate.setHours(h, m, 0)))
      .then(() => console.log(this.state.selectedDate))
  }

  setSelectedDate(date) {
    return new Promise(resolve => {
      this.setState({
        selectedDate: date,
        selectedMonth: new Date(new Date(date).setDate(1))
      }, () => resolve())
    })
  }

  nextMonth() {
    const d = new Date(new Date(this.state.selectedMonth).setDate(1))
    this.setState(
      { selectedMonth: new Date(d.setMonth(d.getMonth() + 1)) },
      () => this.buildMonthTable()
    )
  }

  prevMonth() {
    const d = new Date(new Date(this.state.selectedMonth).setDate(1))
    this.setState(
      { selectedMonth: new Date(d.setMonth(d.getMonth() - 1)) },
      () => this.buildMonthTable()
    )
  }

  _renderRows() {
    return this.state.monthTable.map((row, i) => {
      return (
        <div className='react-range-datepicker-number-row' key={i}>
          {this._renderNumbers(row)}
        </div>
      )
    })
  }

  _renderNumbers(row) {
    return row.map((number, i) => {
      const checkSelectedMonth = () => {
        return (
          new Date(number).getMonth() === this.state.selectedMonth.getMonth()
        )
      }

      const checkSelectedDate = () => {
        return (
          new Date(number).getDate() === this.state.selectedDate.getDate() &&
          new Date(number).getMonth() === this.state.selectedDate.getMonth() &&
          new Date(number).getFullYear() === this.state.selectedDate.getFullYear()
        )
      }

      const checkTodayDate = () => {
        return (
          new Date().getDate() === new Date(number).getDate() &&
          new Date().getMonth() === new Date(number).getMonth() &&
          new Date().getFullYear() === new Date(number).getFullYear()
        )
      }

      let className = ['react-range-datepicker-number']

      if (!checkSelectedMonth()) {
        className.push('react-range-datepicker-not-present')
      }
      if (checkSelectedDate() && checkSelectedMonth()) {
        className.push('react-range-datepicker-selected')
      }

      if (checkTodayDate()) {
        className.push('react-range-datepicker-today')
      }

      if (
        new Date(number).setHours(0, 0, 0, 0) >= new Date(this.state.dateTimeStart).setHours(0, 0, 0, 0) &&
        new Date(number).setHours(0, 0, 0, 0) <= new Date(this.state.dateTimeEnd).getTime()
      ) {
        className.push('react-range-datepicker-in-range')
      }

      return (
        <div
          className={className.join(' ')}
          key={i}
          onClick={() => this.onDate(new Date(number))}
        >
          {new Date(number).getDate()}
        </div>
      )
    })
  }

  onDate(date) {
    if (
      (date.getMonth() === this.state.selectedMonth.getMonth() - 1) ||
      ((date.getMonth() === 11) && (this.state.selectedMonth.getMonth() === 0))
    ) {
      this.prevMonth()
    } else if (
      (date.getMonth() === this.state.selectedMonth.getMonth() + 1) ||
      ((date.getMonth() === 0) && (this.state.selectedMonth.getMonth() === 11))
    ) {
      this.nextMonth()
    }

    this.onSelectDateTime(date)
    this.setSelectedDate(date)
    this.setDateTime(date)
  }

  setDateTime(date) {
    if (this.state.focusedInput === 'start') {
      this.setState({
        dateTimeStart: date,
        timeStringStart: this.props.onlyDate ?
          this.getDateInString(date) : this.getTimeInString(date)
      })
    } else if (this.state.focusedInput === 'end') {
      this.setState({
        dateTimeEnd: date,
        timeStringEnd: this.props.onlyDate ?
          this.getDateInString(date) : this.getTimeInString(date)
      })
    }
  }

  _renderDaysNames() {
    const daysNames = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']

    return daysNames.map((dayName, i) => {
      return (
        <div className='react-range-datepicker-day-name' key={i}>{dayName}</div>
      )
    })
  }

  _renderCalendar() {
    if (!this.state.calendarShow) return null
    return (
      <div className='react-range-datepicker-calendar'>
        <div className='react-range-datepicker-header-block'>
          <div className='react-range-datepicker-header'>
            <button
              className='react-range-datepicker-button react-range-datepicker-left'
              onClick={() => this.prevMonth()}>&#60;</button>
            <div className='react-range-datepicker-date-container'>
              <div className='react-range-datepicker-date'>
                <span className='react-range-datepicker-month'>
                  <Moment format='MMMM' date={this.state.selectedMonth} />
                </span>
                &nbsp;
                <span className='react-range-datepicker-year'>
                  <Moment format='YYYY' date={this.state.selectedMonth} />
                </span>
              </div>
            </div>
            <button
              className='react-range-datepicker-button react-range-datepicker-right'
              onClick={() => this.nextMonth()}>&#62;</button>
          </div>
        </div>
        <div className='react-range-datepicker-days-names-block'>
          <div className='react-range-datepicker-days-names-container'>
            <div className='react-range-datepicker-days-names'>{this._renderDaysNames()}</div>
          </div>
        </div>
        <div className='react-range-datepicker-numbers-block'>
          <div className='react-range-datepicker-numbers-container'>
            {this._renderRows()}
          </div>
        </div>
      </div>
    )
  }

  checkTime(val) {
    return /^(00|10|11|12|[0-1][0-9]|2[0-3]):[0-5][0-9]$/.test(val)
  }

  getCorrectDateTimeFormat(time) {
    const h = time.split(':')[0]
    const m = time.split(':')[1]
    return new Date(this.state.selectedDate.setHours(h, m, 0))
  }

  getTimeInString(datetime) {
    return moment(datetime).format('HH:mm')
  }

  getDateInString(datetime) {
    return moment(datetime).format('DD.MM.YYYY')
  }

  onStartInput(val) {
    if (this.props.onlyDate) return null
    this.setState({ timeStringStart: val })
    if (this.checkTime(val)) {
      this.setState({ dateTimeStart: this.getCorrectDateTimeFormat(val) })
      this.onSelectDateTime(this.getCorrectDateTimeFormat(val))
    }
  }

  onEndInput(val) {
    if (this.props.onlyDate) return null
    this.setState({ timeStringEnd: val })
    if (this.checkTime(val)) {
      this.setState({ dateTimeEnd: this.getCorrectDateTimeFormat(val) })
      this.onSelectDateTime(this.getCorrectDateTimeFormat(val))
    }
  }

  onSelectDateTime(date) {
    if (this.state.focusedInput === 'start') {
      if (this.props.onSelectDateTimeStart) this.props.onSelectDateTimeStart(date)
    }

    if (this.state.focusedInput === 'end') {
      if (this.props.onSelectDateTimeEnd) this.props.onSelectDateTimeEnd(date)
    }
  }

  check(e) {
    const okValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':']
    if (e.key === ':' && e.target.value.indexOf(':') !== -1) e.preventDefault()
    if (okValues.indexOf(e.key) === -1) e.preventDefault()
  }

  showCalendar(timeStringKey, dateTimeKey) {
    this.setState({ focusedInput: timeStringKey })
    this.setSelectedDate(this.state[dateTimeKey])
      .then(() =>
        this.setState({ calendarShow: true }, () => this.buildMonthTable())
      )
  }

  _renderInputs() {
    return (
      <div className='react-range-datepicker-inputs'>
        <input
          size={this.props.onlyDate ? 10 : 5}
          maxLength={5}
          value={this.state.timeStringStart}
          onChange={d => this.onStartInput(d.target.value)}
          onKeyPress={e => this.check(e)}
          onFocus={() => this.showCalendar('start', 'dateTimeStart')}
        />
        &nbsp;-&nbsp;
        <input
          size={this.props.onlyDate ? 10 : 5}
          maxLength={5}
          value={this.state.timeStringEnd}
          onChange={d => this.onEndInput(d.target.value)}
          onKeyPress={e => this.check(e)}
          onFocus={() => this.showCalendar('end', 'dateTimeEnd')}
        />
      </div>
    )
  }

  render() {
    return (
      <div className='react-range-datepicker-container' ref={this.setWrapperRef}>
        {this._renderInputs()}
        {this._renderCalendar()}
      </div>
    )
  }
}
export default ReactRangeDatepicker
